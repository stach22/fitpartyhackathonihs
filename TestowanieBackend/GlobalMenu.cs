﻿using FitParty.Interfaces;
using FitParty.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestowanieBackend
{
    internal class GlobalMenu
    {
        IDrinkService _drinkService;

        public void Run()
        {
            PrintList();
            Console.WriteLine("===========================================================");
            AddList();
            Console.WriteLine("===========================================================");
            PrintList();
        }

        public void PrintList()
        {
            var drinkList = _drinkService.GetDrinkList();

            foreach (var drink in drinkList)
            {
                Console.WriteLine($"Name: {drink.Name}, calories per portion: {drink.CaloriesPerPortion}" +
                    $", volume of the portion: {drink.VolumeOfThePortionInGrams}, score: {drink.Score}");
            }
        }

        public void AddList()
        {
            Console.WriteLine("Enter name of the drink");
            var drinkName = Console.ReadLine();

            Console.WriteLine("Enter calories per portion");
            var calories = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Enter volume of the portion [g]");
            var volume = Convert.ToDouble(Console.ReadLine());

            try
            {
                _drinkService.AddToTheListCustomDrink(drinkName, calories, volume);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
