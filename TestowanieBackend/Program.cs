﻿using FitParty.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestowanieBackend
{
    class Program
    {
        private GlobalMenu _globalMenu = new GlobalMenu();

        static void Main(string[] args)
        {
            var program = new Program();
            program.Start();

            Console.ReadKey();
        }

        public void Start()
        {
            _globalMenu.Run();
        }
    }
}
