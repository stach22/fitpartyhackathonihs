﻿using FitParty.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitParty.Services;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static FitParty.Services.GoogleMapsApi;

namespace FitParty
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChoosePlacePage : ContentPage
    {
        private IStepCounter _stepCounter;
        private RootObject _places;
        private PartyConfig _partyConfig;

        public ChoosePlacePage()
        {
            InitializeComponent();
        }

        public ChoosePlacePage(RootObject places, PartyConfig partyConfig, IStepCounter stepCounter)
        {
            InitializeComponent();

            _places = places;
            _partyConfig = partyConfig;
            _stepCounter = stepCounter;

            InsertFramesWithLocations();
        }

        private void InsertFramesWithLocations()
        {
            foreach (var place in _places.Results)
            {
                FrameStack.Children.Add(CreateFrameWithPlace(place));
            }
        }

        private Frame CreateFrameWithPlace(Result place)
        {
            Frame frame = new Frame
            {
                BorderColor = Color.FromHex("#f3ab10"),
                BackgroundColor = Color.Transparent,
                Margin = new Thickness(10),
                CornerRadius = 15
            };

            Grid grid = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto },
                    new RowDefinition { Height = GridLength.Auto }
                },

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star }
                }
            };

            Label nameLabel = new Label
            {
                Text = place.Name,
                HorizontalOptions = LayoutOptions.Center,
                FontSize = 20,
                TextColor = Color.White
            };

            Label addressLabel = new Label
            {
                Text = place.Vicinity,
                TextColor = Color.White
            };

            Xamarin.Essentials.Location placeLocation = new Xamarin.Essentials.Location
            {
                Latitude = place.Geometry.Location.Lat,
                Longitude = place.Geometry.Location.Lng
            };

            var distance = Xamarin.Essentials.Location.CalculateDistance(_partyConfig.UserLocation,
                                                                            placeLocation,
                                                                            DistanceUnits.Kilometers);

            Label distanceLabel = new Label
            {
                Text = distance.ToString("0.00") + "km",
                HorizontalOptions = LayoutOptions.End,
                TextColor = Color.White
            };



            Label isOpenLabel = GetOpeningHoursLabel(place);

            Button goThereButton = new Button
            {
                Text = "Go There",
                ClassId = _places.Results.FindIndex(x => x == place).ToString()
            };

            goThereButton.Clicked += GoThere_Clicked;

            grid.Children.Add(nameLabel, 0, 2, 0, 1);
            grid.Children.Add(addressLabel, 0, 1);
            grid.Children.Add(distanceLabel, 1, 1);
            grid.Children.Add(isOpenLabel, 0, 2);
            grid.Children.Add(goThereButton, 1, 2);

            frame.Content = grid;

            return frame;
        }

        private Label GetOpeningHoursLabel(Result place)
        {
            if (place.Opening_hours == null)
            {
                return new Label
                {
                    Text = "Unkown Opening Hours",
                    TextColor = Color.White
                };
            }

            if (place.Opening_hours.Open_now)
            {
                return new Label
                {
                    Text = "Open Now",
                    TextColor = Color.White
                };
            }
            else
            {
                return new Label
                {
                    Text = "Closed",
                    TextColor = Color.Red
                };
            }
        }

        private async void GoThere_Clicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var place = _places.Results[int.Parse(button.ClassId)];
            ConfigPartyPage.Party.Pubs.Add(place);
            await Map.OpenAsync(place.Geometry.Location.Lat, place.Geometry.Location.Lng,
                new MapLaunchOptions { Name = place.Name });

            await Navigation.PushAsync(new DuringTripPage(place, _places, _partyConfig, _stepCounter));
        }



    }
}