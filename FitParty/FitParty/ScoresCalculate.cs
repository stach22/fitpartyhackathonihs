﻿using FitParty.Helpers;
using FitParty.Interfaces;
using FitParty.Models;
using FitParty.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitParty
{
    public class ScoresCalculate
    {
        IDrinkService _drinkService;
        private readonly Conversion _conversion = new Conversion();
        private readonly Trip _trip = new Trip();

        public ScoresCalculate(IDrinkService drinkService)
        {
            _drinkService = drinkService;
        }

        private List<int> _burntCaloriesPerPerson = new List<int>();

        public int CalculateDrinkCalories(Dictionary<string, int> DrinkenDrinks)
        {
            var drinkList = _drinkService.ReadyDrinkList();
            var caloriesList = new List<int>();

            var caloriesOfDrink = 0;

            foreach (var item in drinkList)
            {
                var drinkNumber = DrinkenDrinks.SingleOrDefault(x => x.Key == item.Name).Value;
                if (DrinkenDrinks.Keys.ToString() == item.Name)
                {
                    var calories = Convert.ToInt32(item.CaloriesPerPortion * drinkNumber);
                    caloriesList.Add(calories);
                }
            }
            caloriesOfDrink = caloriesList.Sum();

            return caloriesOfDrink;
        }

        public int GetScorePerParty(int burntCalories)
        {
            var scores = 0;

            var caloriesFromDrink = CalculateDrinkCalories(PartyConfig.DrinkenDrinks);

            var calories = caloriesFromDrink - burntCalories;

            if (calories >= 0)
            {
                scores = 0;
                return scores;
            }

            if (calories < 0)
            {
                scores = -calories;
                return scores;
            }

            return scores;
        }
    }
}
