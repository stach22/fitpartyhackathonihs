﻿using FitParty.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitParty.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FitParty
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HistoricalPartiesPage : ContentPage
	{
        private List<Party> _parties;

        public HistoricalPartiesPage()
        {
            InitializeComponent();

            if (new HistoryService().ReturnHistory() == null)
            {
                NotFoundButton.IsVisible = true;
                return;
            }
            
            InsertButtonsWithParties();
        }

        private void InsertButtonsWithParties()
        {
            _parties = new HistoryService().ReturnHistory();

            foreach (var party in _parties)
            {
                FrameStack.Children.Add(CreateButton(party));
            }
        }

        private Button CreateButton(Party party)
        {
            Button goThereButton = new Button
            {
                Text = party.DateParty.ToShortDateString(),
                ClassId = _parties.IndexOf(party).ToString(),
                BackgroundColor = Color.Transparent,
                CornerRadius = 10,
                BorderWidth = 3,
                BorderColor = Color.FromHex("#f3ab10"),
                TextColor = Color.White,
                Margin = new Thickness(15)
            };

            goThereButton.Clicked += GoThere_Clicked;

            return goThereButton;
        }

        private async void GoThere_Clicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var party = _parties[int.Parse(button.ClassId)];

            await Navigation.PushAsync(new ShowHistory(party));
        }
    }
}