﻿using FitParty.Helpers;
using FitParty.Interfaces;
using FitParty.Models;
using FitParty.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static FitParty.Services.GoogleMapsApi;

namespace FitParty
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivePartyRelaxPage : ContentPage
    {
        private Result _place;
        private IDrinkService _drinkService;
        private Conversion _conversion = new Conversion();
        private PartyService _partyService = new PartyService();
        private readonly IStepCounter _stepCounter;
        private readonly Trip _trip;
        private List<Grid> _grids;
        private ScoresCalculate _scoresCalculate;

        private RootObject _apiPlaces;
        private PartyConfig _partyConfig;
        private GoogleMapsApi _googleMapsApi;

        public ActivePartyRelaxPage()
        {
            InitializeComponent();
        }

        public ActivePartyRelaxPage(Result place, RootObject apiPlaces, PartyConfig partyConfig, IStepCounter stepCounter, Trip trip)
        {
            InitializeComponent();

            _drinkService = new DrinkService();
            _scoresCalculate = new ScoresCalculate(_drinkService);
            _place = place;
            _grids = new List<Grid>();
            _googleMapsApi = new GoogleMapsApi();

            _apiPlaces = apiPlaces;
            _partyConfig = partyConfig;
            _stepCounter = stepCounter;
            _trip = trip;

            UserNameButton.Text = App.ActiveUser.Name;

            PopulateListOfDrinks();
        }

        private void PopulateListOfDrinks()
        {
            var drinks = _drinkService.ReadyDrinkList();

            if (PartyConfig.DrinkenDrinks == null)
            {
                PartyConfig.DrinkenDrinks = new Dictionary<string, int>();
            }

            foreach (var drink in drinks)
            {
                var newGrid = AddGridWithDrink(drink);
                DrinksStack.Children.Add(newGrid);
                _grids.Add(newGrid);
            }

            if (PartyConfig.DrinkenDrinks != null)
            {
                UpdateListOfDrinks();
            }
        }

        private void UpdateListOfDrinks()
        {
            foreach (var drink in PartyConfig.DrinkenDrinks)
            {
                var findGrid = _grids.FirstOrDefault(x => x.ClassId == drink.Key);

                var findCountingLabel = findGrid.Children.Where(x => x.ClassId == findGrid.ClassId).FirstOrDefault() as Label;

                findCountingLabel.Text = drink.Value.ToString();
            }
        }

        private Grid AddGridWithDrink(Drink drink)
        {
            Grid grid = new Grid
            {
                ClassId = drink.Name,

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Auto },
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Auto }

                },

                RowDefinitions =
                {
                    new RowDefinition { Height = GridLength.Auto },
                },

                Margin = new Thickness(15)
            };

            Label drinkCounter = new Label
            {
                ClassId = drink.Name,
                Text = "0",
                TextColor = Color.White,
                FontSize = 25,
                VerticalOptions = LayoutOptions.Center
            };

            Image drinkIcon = new Image
            {
                Source = drink.IconPath,
                WidthRequest = 40
            };

            Label drinkName = new Label
            {
                Text = drink.Name,
                FontSize = 20,
                VerticalOptions = LayoutOptions.Center,
                TextColor = Color.White
            };

            Image plusImage = new Image
            {
                Source = "plus",
                WidthRequest = 40,
                HeightRequest = 40
                
            };

            Button addOneButton = new Button
            {
                BackgroundColor = Color.Transparent,
                ClassId = drink.Name
            };

            addOneButton.Clicked += AddOne_Clicked;

            grid.Children.Add(drinkCounter, 0, 0);
            grid.Children.Add(drinkIcon, 1, 0);
            grid.Children.Add(drinkName, 2, 0);
            grid.Children.Add(plusImage, 3, 0);
            grid.Children.Add(addOneButton, 3, 0);

            return grid;
        }

        // najgłupsza rzecz w xamarinie
        // instrukcja w jaki sposób dodawać elementy do Grida po stronie kodu
        // left, right, top, bottom

        //* setColumn(left) , setColumnSpan(right - left = 1), setRow(top), setRowSpan(bottom - top = 2)

        // ex. grid.Children.Add(volumenLabel, 1, 2, 0, 2);

        private void AddOne_Clicked(object sender, EventArgs e)
        {
            var button = (Button)sender;

            var drinkName = button.ClassId;

            var findGrid = _grids.FirstOrDefault(x => x.ClassId == drinkName);

            var findCountingLabel = findGrid.Children.Where(x => x.ClassId == findGrid.ClassId).FirstOrDefault() as Label;

            var findDrinkInDict = PartyConfig.DrinkenDrinks.FirstOrDefault(x => x.Key == drinkName).Key;

            if (findDrinkInDict == null)
            {
                PartyConfig.DrinkenDrinks.Add(drinkName, 1);

                findCountingLabel.Text = "1";
            }
            else
            {
                var oldValue = PartyConfig.DrinkenDrinks[drinkName];

                PartyConfig.DrinkenDrinks[drinkName] = oldValue + 1;

                var newValue = PartyConfig.DrinkenDrinks[drinkName];

                findCountingLabel.Text = newValue.ToString();
            }


        }

        private async void FinishPartyButton_Clicked(object sender, EventArgs e)
        {
            ConfigPartyPage.Party.Steps = _stepCounter.Steps;
            ConfigPartyPage.Party.Distance = _conversion.GetDistanceInKm(ConfigPartyPage.Party.Steps);
            ConfigPartyPage.Party.Points = _scoresCalculate.GetScorePerParty(ConfigPartyPage.Party.CaloriesBurnt);
            _partyService.AddParty(ConfigPartyPage.Party);

            await Navigation.PushAsync(new MainPage(_stepCounter));
        }

        private async void AnotherPlaceButton_Clicked(object sender, EventArgs e)
        {
            GetUsersLocation();

            if (!SearchForPlaces())
            {
                await DisplayAlert("Warning", "No places found in desired location. Provide different Configuration", "OK");

                return;
            }

            await Navigation.PushAsync(new ChoosePlacePage(_apiPlaces, _partyConfig, _stepCounter));

        }

        private async void GetUsersLocation()
        {
            var location = await Geolocation.GetLastKnownLocationAsync();

            _partyConfig.UserLocation = location;



        }

        private bool SearchForPlaces()
        {
            RootObject response = _googleMapsApi.GetResponse(_partyConfig);

            if (response.Results.Count == 0)
            {
                return false;
            }

            _apiPlaces = response;

            return true;
        }
    }
}