﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace FitParty.Helpers
{
    public enum PartyType
    {
        Relax,
        Survival
    }

    public class PartyConfig
    {
        public static Dictionary<string, int> DrinkenDrinks;

        public Location UserLocation { get; set; }
        public int Radius { get; set; }

    }

    public static class DistanceCounter
    {
        public static bool IsUserOnPlace(Location desiredLocation)
        {
            var currentLocation = Geolocation.GetLastKnownLocationAsync();

            var distance = Xamarin.Essentials.Location.CalculateDistance(currentLocation.Result, desiredLocation, DistanceUnits.Kilometers);

            if (distance < 0.1)
            {
                return true;
            }

            return false;
        }
    }
}
