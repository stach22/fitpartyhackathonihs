﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Auth;

namespace FitParty
{
   public class FacebookAuthentication
   {
        //private const string ClientId = "2247624358655576";
        private const string ClientId = "julek19@interia.eu";

        private const string Scope = "email";
        private const string AuthorizeUrl = "https://www.facebook.com/dialog/oauth/";
        private const string RedirectUrl = "https://www.facebook.com/connect/login_success.html";
        private const bool IsUsingNativeUI = false;

        private OAuth2Authenticator _auth;
        private  readonly IFacebookAuthenticationDelegate _authenticationDelegate;

        public FacebookAuthentication(IFacebookAuthenticationDelegate authenticationDelegate)
        {
            _authenticationDelegate = authenticationDelegate;

            _auth = new OAuth2Authenticator(ClientId, Scope,
                new Uri(AuthorizeUrl),
                new Uri(RedirectUrl),
                null, IsUsingNativeUI);

            _auth.Completed += OnAuthenticationCompleted;
            _auth.Error += OnAuthenticationFailed;
        }

        public OAuth2Authenticator GetAuthenticator()
        {
            return _auth;
        }

        public void OnPageLoading(Uri uri)
        {
            _auth.OnPageLoading(uri);
        }

        private void OnAuthenticationCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            if (e.IsAuthenticated)
            {
                var token = new FacebookToken()
                {
                    AccessToken = e.Account.Properties["access_token"]
                };
                _authenticationDelegate.OnAuthenticationCompleted(token);
            }
            else
            {
                _authenticationDelegate.OnAuthenticationCanceled();
            }
        }

        private void OnAuthenticationFailed(object sender, AuthenticatorErrorEventArgs e)
        {
            _authenticationDelegate.OnAuthenticationFailed(e.Message, e.Exception);
        }
    }
}
