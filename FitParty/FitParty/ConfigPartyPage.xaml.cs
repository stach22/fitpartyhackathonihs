﻿using FitParty.Helpers;
using FitParty.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitParty.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static FitParty.Services.GoogleMapsApi;

namespace FitParty
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfigPartyPage : ContentPage
    {
        private IStepCounter _stepCounter;
        private GoogleMapsApi _googleMapsApi;
        private RootObject _apiPlaces;
        public static Party Party;
        private PartyService _partyService = new PartyService();

        public ConfigPartyPage()
        {
            InitializeComponent();

        }

        public ConfigPartyPage(IStepCounter stepCounter)
        {
            InitializeComponent();

            _googleMapsApi = new GoogleMapsApi();
            _stepCounter = stepCounter;

            GetUsersLocation();
        }

        private async void BeginPartyButton_Clicked(object sender, EventArgs e)
        {
            if (!IsRadiusCorrect())
            {
                await DisplayAlert("Warning", "Wrong value in radius. Provide natural value without coma", "OK");
                return;
            }

            var partyConfig = new PartyConfig
            {
                UserLocation = await Geolocation.GetLastKnownLocationAsync(),
                Radius = int.Parse(RadiusEntry.Text)
            };

            Party = new Party
            {
                DateParty = DateTime.Now,
                Pubs = new List<Result>()
            };

            if (!SearchForPlaces(partyConfig))
            {
                await DisplayAlert("Warning", "No places found in desired location. Provide different Configuration", "OK");

                return;
            }

            switch (StartPartyPage.PartyMode)
            {
                case PartyType.Relax:
                    await Navigation.PushAsync(new ChoosePlacePage(_apiPlaces, partyConfig, _stepCounter));
                    break;
                case PartyType.Survival:
                    // begin survivial party
                    var randomPlace = SelectRandomPlace();
                    OpenNaviInstantly(randomPlace, partyConfig);
                    break;
            }
        }

        private async void OpenNaviInstantly(Result randomPlace, PartyConfig partyConfig)
        {
            await Map.OpenAsync(randomPlace.Geometry.Location.Lat, randomPlace.Geometry.Location.Lng, new MapLaunchOptions
            {
                Name = randomPlace.Name,
                NavigationMode = NavigationMode.Walking
            });


            await Navigation.PushAsync(new DuringTripPage(randomPlace, _apiPlaces, partyConfig, _stepCounter));
        }

        private Result SelectRandomPlace()
        {
            var random = new Random();

            var index = random.Next(_apiPlaces.Results.Count);

            return _apiPlaces.Results[index];
        }

        private bool SearchForPlaces(PartyConfig partyConfig)
        {
            RootObject response = _googleMapsApi.GetResponse(partyConfig);

            if (response.Results.Count == 0)
            {
                return false;
            }

            _apiPlaces = response;

            return true;
        }

        private async void GetUsersLocation()
        {
            var location = await Geolocation.GetLastKnownLocationAsync();

            LocationLabel.Text = $"{location.Latitude} , {location.Longitude}";
        }

        private bool IsRadiusCorrect()
        {
            return int.TryParse(RadiusEntry.Text, out int radius);
        }

        private void AddFriendButton_Clicked(object sender, EventArgs e)
        {

        }
    }
}