﻿using System;
using System.Threading.Tasks;
using FitParty.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FitParty
{
    public partial class App : Application
    {
        private IStepCounter _stepCounter;
        private UserService _userService;
        public static User ActiveUser;

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new LoginPage());
        }

        public App(IStepCounter stepCounter)
        {
            _stepCounter = stepCounter;
            InitializeComponent();

            _userService = new UserService();

            var userExist = _userService.CheckUserExist();

            if (userExist.Status == TaskStatus.Faulted)
            {
                MainPage = new NavigationPage(new LoginPage(_stepCounter));
            }
            else
            {
                ActiveUser = userExist.Result;

                MainPage = new NavigationPage(new MainPage(_stepCounter));
            }

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
