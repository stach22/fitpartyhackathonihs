﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FitParty.Model;
using FitParty.Services;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static FitParty.Services.GoogleMapsApi;

namespace FitParty
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShowHistory : ContentPage
    {
        private Party _party;

        public ShowHistory()
        {
            InitializeComponent();
        }

        public ShowHistory(Party party)
        {
            InitializeComponent();
            _party = party;
            GetPartyHistory();
        }

        private void GetPartyHistory()
        {
            PartyDateLabel.Text = _party.DateParty.ToShortDateString();
            DistanceLabel.Text = $"{_party.Distance.ToString("0.00")} km";
            StepsLabel.Text = _party.Steps.ToString();
            CaloriesLabel.Text = _party.CaloriesBurnt.ToString();
            PointsLabel.Text = _party.Points.ToString();

            var localsList = _party.Pubs;

            foreach (var local in localsList)
            {
                LocalsStack.Children.Add(CreateFrame(local));
            }
        }

        private Frame CreateFrame(Result local)
        {
            Frame frame = new Frame
            {
                BorderColor = Color.FromHex("#f3ab10"),
                BackgroundColor = Color.Transparent,
                Margin = new Thickness(5),
                CornerRadius = 15
            };

            Grid grid = new Grid
            {
                RowDefinitions =
                {
                    new RowDefinition { Height = GridLength.Auto }
                },

                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = GridLength.Star },
                    new ColumnDefinition { Width = GridLength.Star }
                }
            };

            Label localNameLabel = new Label()
            {
                Text = local.Name,
                HorizontalOptions = LayoutOptions.Center,
                FontSize = 20,
                TextColor = Color.White
            };

            Label localCityLabel = new Label()
            {
                Text = local.Vicinity,
                HorizontalOptions = LayoutOptions.Center,
                FontSize = 20,
                TextColor = Color.White
            };



            // najgłupsza rzecz w xamarinie
            // instrukcja w jaki sposób dodawać elementy do Grida po stronie kodu
            // left, right, top, bottom

            //* setColumn(left) , setColumnSpan(right - left = 1), setRow(top), setRowSpan(bottom - top = 2)

            // ex. grid.Children.Add(volumenLabel, 1, 2, 0, 2);
            grid.Children.Add(localNameLabel, 0, 0);
            grid.Children.Add(localCityLabel, 1, 0);

            frame.Content = grid;

            return frame;
        }
    }
}