﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitParty.Services;
using Xamarin.Forms;

namespace FitParty
{
    public partial class MainPage : ContentPage
    {
        private IStepCounter _stepCounter;

        public MainPage()
        {
            InitializeComponent();

            Title = "Main Menu";
        }

        public MainPage(IStepCounter stepCounter)
        {
            _stepCounter = stepCounter;
            InitializeComponent();

            Title = "Main Menu";
        }

        private async void StartPartyButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new StartPartyPage(_stepCounter));

        }

        private async void PartiesHistoryButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new HistoricalPartiesPage());
        }

        //private void SettingsButton_Clicked(object sender, EventArgs e)
        //{

        //}
    }
}
