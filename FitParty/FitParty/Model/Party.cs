﻿using System;
using System.Collections.Generic;
using System.Text;

using FitParty.Model;
using FitParty.Services;

namespace FitParty
{
    public class Party
    {
        public int Id { get; set; }
        public DateTime DateParty { get; set; }
        //public List<Friend> Friends { get; set; }
        public double Distance { get; set; }
        public int Steps { get; set; }
        public int CaloriesBurnt { get; set; }
        public int Points { get; set; }
        public List<GoogleMapsApi.Result> Pubs { get; set; }
    }
}

