﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FitParty
{
   public class Friend
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Weight { get; set; }
    }
}
