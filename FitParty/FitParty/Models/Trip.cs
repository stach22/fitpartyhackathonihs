﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FitParty.Models
{
    public class Trip
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
