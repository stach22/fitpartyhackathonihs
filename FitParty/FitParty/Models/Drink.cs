﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FitParty.Models
{
    public class Drink
    {
        public string Name { get; set; }
        public double CaloriesPerPortion { get; set; }
        public double VolumeOfThePortionInGrams { get; set; }
        public string IconPath { get; set; }
    }
}
