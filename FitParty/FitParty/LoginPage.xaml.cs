﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitParty.Services;
using FitParty.Facebook;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FitParty
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
    {
        private IStepCounter _stepCounter;

		public LoginPage ()
		{
			InitializeComponent ();
        }

        public LoginPage(IStepCounter stepCounter)
        {
            _stepCounter = stepCounter;
            InitializeComponent();
        }

        private async void AddUserButton_Clicked(object sender, EventArgs e)
        {
            var userService = new UserService();

            App.ActiveUser = new User
            {
                Name = UserNameEntry.Text,
                Weight = int.Parse(WeightEntry.Text)
            };

            await userService.AddUser(App.ActiveUser.Name, App.ActiveUser.Weight);

            await Navigation.PushAsync(new MainPage(_stepCounter));
        }
    }
}