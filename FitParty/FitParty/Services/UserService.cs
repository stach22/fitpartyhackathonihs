﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace FitParty.Services
{
    public class UserService
    {
        public async Task<User> AddUser(string name,int weight)
        {
            var user = new User()
            {
                Name = name,
                Weight = weight
            };

            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = Path.Combine(documentsPath, "UserList.json");

            var userToJson = JsonConvert.SerializeObject(user, Formatting.Indented);
            File.AppendAllText(filePath, userToJson);
            return user;
        }
        public async Task<User> CheckUserExist()
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = Path.Combine(documentsPath, "UserList.json");          

            if (File.Exists(filePath))
            {
                var users = File.ReadAllText(filePath);
                if (users == "")
                {
                    return null;
                }
            }

            var userToReturn = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<User>(userToReturn);
        }
    }
}
