﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.TizenSpecific;

namespace FitParty.Services
{
    public class Conversion
    {
        public int GetCaloriesBurnt(DateTime start, DateTime end, int weight)
        {
            return Convert.ToInt32(weight * 3 * (end - start).TotalHours);
        }

        public double GetDistanceInKm(int steps)
        {
            return steps * 0.000762;
        }
    }
}
