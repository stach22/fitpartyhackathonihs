﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using FitParty.Model;

using Newtonsoft.Json;

using Xamarin.Essentials;

namespace FitParty.Services
{
    public class PartyService
    {
        public async void AddParty(Party party)
        {
            var listParty = new List<Party>();
            listParty.Add(party);

            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = Path.Combine(documentsPath, "PersonList.json");

            if (File.Exists(filePath))
            {
                var parties = File.ReadAllText(filePath);
                var convertedListPartyToJson = JsonConvert.SerializeObject(listParty, Formatting.Indented);

                if (parties == "")
                {
                    File.AppendAllText(filePath, convertedListPartyToJson);
                    return;
                }

                var listOfHistories = JsonConvert.DeserializeObject<List<Party>>(parties);

                listOfHistories.Add(party);

                var convertedJson = JsonConvert.SerializeObject(listOfHistories, Formatting.Indented);

                if (parties == "")
                {
                    File.AppendAllText(filePath, convertedListPartyToJson);
                    return;
                }


                File.WriteAllText(filePath, convertedJson);

            }
            else
            {
                var convertedJson = JsonConvert.SerializeObject(listParty, Formatting.Indented);

                File.WriteAllText(filePath, convertedJson);
            }


            

            


        }
    }
}
