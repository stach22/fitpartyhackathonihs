﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FitParty.Services
{
    public interface IStepCounter
    {
        int Steps { get; set; }
        void InitSensorService();
        bool IsAvailable();
        bool IsInitialized { get; set; }
        void StopSensorService();
    }
}
