﻿using FitParty.Interfaces;
using FitParty.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FitParty.Services
{
    public class DrinkService : IDrinkService
    {
        private static List<Drink> drinksList = new List<Drink>();
        private static List<Drink> _chosenDrinkList = new List<Drink>();


        public List<Drink> ReadyDrinkList()
        {
            var drinks = new List<Drink>()
            {
                new Drink {Name = "Light Beer", CaloriesPerPortion = 245.0, VolumeOfThePortionInGrams = 500.0, IconPath = "ico_beer.png"},
                new Drink {Name = "Porter Beer", CaloriesPerPortion = 345.0, VolumeOfThePortionInGrams = 500.0, IconPath = "ico_beer.png"},
                new Drink {Name = "Alcohol Free Beer", CaloriesPerPortion = 105.0, VolumeOfThePortionInGrams = 500.0, IconPath = "ico_beer.png"},
                new Drink {Name = "Karmi Classic", CaloriesPerPortion = 200.0, VolumeOfThePortionInGrams = 400.0, IconPath = "ico_beer.png"},
                new Drink {Name = "Desperados", CaloriesPerPortion = 232.0, VolumeOfThePortionInGrams = 400.0, IconPath = "ico_beer.png"},
                new Drink {Name = "Redds Apple", CaloriesPerPortion = 270.0, VolumeOfThePortionInGrams = 500.0, IconPath = "ico_beer.png"},
                new Drink {Name = "Cider", CaloriesPerPortion = 112, VolumeOfThePortionInGrams = 240.0, IconPath = "ico_cider.png"},
                new Drink {Name = "Vodka", CaloriesPerPortion = 110.0, VolumeOfThePortionInGrams = 50.0, IconPath = "ico_vodka.png"},
                new Drink {Name = "Advocaat", CaloriesPerPortion = 140.0, VolumeOfThePortionInGrams = 50.0},
                new Drink {Name = "Rum", CaloriesPerPortion = 115.0, VolumeOfThePortionInGrams = 50.0, IconPath = "ico_rum.png"},
                new Drink {Name = "Whisky", CaloriesPerPortion = 110.0, VolumeOfThePortionInGrams = 50.0, IconPath = "ico_whisky.png"},
                new Drink {Name = "Sparkling Wine", CaloriesPerPortion = 76.0, VolumeOfThePortionInGrams = 100.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Sweet white wine", CaloriesPerPortion = 114.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Semi sweet white wine", CaloriesPerPortion = 103.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Semi dry white wine", CaloriesPerPortion = 97.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Dry white wine", CaloriesPerPortion = 79.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Sweet red wine", CaloriesPerPortion = 128.0, VolumeOfThePortionInGrams = 120.0, IconPath = "/gfxico_wine/.png"},
                new Drink {Name = "Semi sweet red wine", CaloriesPerPortion = 105.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Semi dry red wine", CaloriesPerPortion = 90.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Dry red wine", CaloriesPerPortion = 82.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Sweet vermouth", CaloriesPerPortion = 186.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Dry vermouth", CaloriesPerPortion = 114.0, VolumeOfThePortionInGrams = 120.0, IconPath = "ico_wine.png"},
                new Drink {Name = "Brandy", CaloriesPerPortion = 220.0, VolumeOfThePortionInGrams = 100.0, IconPath = "ico_brandy.png"},
                new Drink {Name = "Champagne", CaloriesPerPortion = 76.0, VolumeOfThePortionInGrams = 100.0, IconPath = "ico_champagne.png"},
                new Drink {Name = "Mojito", CaloriesPerPortion = 190.0, VolumeOfThePortionInGrams = 250.0, IconPath = "ico_mojito.png"},
                new Drink {Name = "Pina Colada", CaloriesPerPortion = 180.0, VolumeOfThePortionInGrams = 150.0, IconPath = "ico_pina_colada.png"},
                new Drink {Name = "Cosmopolitan", CaloriesPerPortion = 147.0, VolumeOfThePortionInGrams = 100.0},
                new Drink {Name = "Cuba Libre", CaloriesPerPortion = 201.0, VolumeOfThePortionInGrams = 240.0},
                new Drink {Name = "Long Island Ice Tea", CaloriesPerPortion = 285.0, VolumeOfThePortionInGrams = 240},
                new Drink {Name = "Bloody Mary", CaloriesPerPortion = 123.0, VolumeOfThePortionInGrams = 150.0},
                new Drink {Name = "Martini", CaloriesPerPortion = 160.0, VolumeOfThePortionInGrams = 70.0},
                new Drink {Name = "Margarita", CaloriesPerPortion = 168.0, VolumeOfThePortionInGrams = 77.0},
                new Drink {Name = "Gin with tonic", CaloriesPerPortion = 171.0, VolumeOfThePortionInGrams = 225.0},
                new Drink {Name = "Daiquiri", CaloriesPerPortion = 111.0, VolumeOfThePortionInGrams = 60.0},
                new Drink {Name = "Sex on the beach", CaloriesPerPortion = 326.0, VolumeOfThePortionInGrams = 225.0},
                new Drink {Name = "Kamikaze", CaloriesPerPortion = 87.0, VolumeOfThePortionInGrams = 50.0},
                new Drink {Name = "Tequila sunrise", CaloriesPerPortion = 232.0, VolumeOfThePortionInGrams = 210.0},
                new Drink {Name = "Black russian", CaloriesPerPortion = 236.0, VolumeOfThePortionInGrams = 90.0},
                new Drink {Name = "B 52", CaloriesPerPortion = 103.0, VolumeOfThePortionInGrams = 30.0},
                new Drink {Name = "Cola", CaloriesPerPortion = 101.0, VolumeOfThePortionInGrams = 240.0, IconPath = "ico_cola.png"},
                new Drink {Name = "Cola Zero", CaloriesPerPortion = 0.0, VolumeOfThePortionInGrams = 240.0, IconPath = "ico_cola.png"},
                new Drink {Name = "Red Bull", CaloriesPerPortion = 113.0, VolumeOfThePortionInGrams = 250.0, IconPath = "ico_red_bull.png"},
                new Drink {Name = "Red Bull Sugarfree", CaloriesPerPortion = 8.0, VolumeOfThePortionInGrams = 250.0, IconPath = "ico_red_bull.png"},
                new Drink {Name = "Burn", CaloriesPerPortion = 140.0, VolumeOfThePortionInGrams = 250.0},
                new Drink {Name = "Tiger", CaloriesPerPortion = 115.0, VolumeOfThePortionInGrams = 250.0},
                new Drink {Name = "Green Up", CaloriesPerPortion = 88.0, VolumeOfThePortionInGrams = 250.0},
                new Drink {Name = "Fritz-kola", CaloriesPerPortion = 139.0, VolumeOfThePortionInGrams = 330.0},
                new Drink {Name = "Cappy orange 100%", CaloriesPerPortion = 139.0, VolumeOfThePortionInGrams = 330.0, IconPath = "ico_orange_juice.png"},
                new Drink {Name = "Lemonade", CaloriesPerPortion = 100.0, VolumeOfThePortionInGrams = 250.0},
                new Drink {Name = "Water", CaloriesPerPortion = 0.0, VolumeOfThePortionInGrams = 250.0, IconPath = "ico_water.png"},
            };

            return drinks;
        }

        public List<Drink> ChosenDrinksPerPerson(Drink drink)
        {
             _chosenDrinkList.Add(drink);

            return _chosenDrinkList;
        }

        public Drink AddCustomDrink(string name, double caloriesPerPortion, double volumeOfThePortionInGrams)
        {
            var drink = new Drink()
            {
                Name = name,
                CaloriesPerPortion = caloriesPerPortion,
                VolumeOfThePortionInGrams = volumeOfThePortionInGrams,
            };

            return drink;
        }

        public void AddToTheListCustomDrink(string name, double caloriesPerPortion, double volumeOfThePortionInGrams)
        {
            var drink = AddCustomDrink(name, caloriesPerPortion, volumeOfThePortionInGrams);
            drinksList = ReadyDrinkList();

            bool doesDrinkNameIsCorrect = DoesDrinkNameIsCorrect(name);
            bool doesCaloriesAndVolumeAndScoreHaveCorrectFormat = DoesCaloriesAndVolumeHaveCorrectFormat(caloriesPerPortion, volumeOfThePortionInGrams);
            bool doesDrinkExist = DoesDrinkExist(name);

            if (doesDrinkNameIsCorrect && doesCaloriesAndVolumeAndScoreHaveCorrectFormat && !doesDrinkExist)
            {
                drinksList.Add(drink);
            }
        }

        public bool DoesDrinkNameIsCorrect(string name)
        {
            bool isCorrect = true;

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Name of the drink is empty!");
            }

            return isCorrect;
        }

        public bool DoesCaloriesAndVolumeHaveCorrectFormat(double caloriesPerPortion, double volumeOfThePortionInGrams)
        {
            bool isCorrect = true;

            var caloriesType = Convert.ToDouble(caloriesPerPortion).GetType();
            var volumeType = Convert.ToDouble(volumeOfThePortionInGrams).GetType();

            if (caloriesType.Name == "double")
            {
                throw new Exception("Calories are not a number");
            }

            if (volumeType.Name == "double")
            {
                throw new Exception("Volume of the portion is not a number");
            }

            return isCorrect;
        }

        public bool DoesDrinkExist(string name)
        {
            bool doesDrinkExist = drinksList.Any(x => x.Name == name);

            if (doesDrinkExist)
            {
                throw new Exception("This drink already exist");
            }

            return doesDrinkExist;
        }

        public List<Drink> GetDrinkList()
        {
            return drinksList;
        }

        public List<Drink> GetChosenDrinkList()
        {
            return _chosenDrinkList;
        }
    }
}
