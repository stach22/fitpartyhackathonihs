﻿namespace FitParty.Services
{
    public class User
    {
        public string Name { get; set; }
        public int Weight { get; set; }
    }
}