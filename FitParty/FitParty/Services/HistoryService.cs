﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Newtonsoft.Json;

namespace FitParty.Services
{
   public class HistoryService
    {
        public List<Party> ReturnHistory()
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = Path.Combine(documentsPath, "PersonList.json");

            if (File.Exists(filePath))
            {
                var parties = File.ReadAllText(filePath);

                if (parties == "")
                {
                    return null;
                }

                var listOfHistories = JsonConvert.DeserializeObject<List<Party>>(parties);

                return listOfHistories;
            }
            else
            {
                return null;
            }
        }
    }
}
