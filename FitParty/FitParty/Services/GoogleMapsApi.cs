﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Text;
using FitParty.Helpers;
using Newtonsoft.Json;

namespace FitParty.Services
{
    public class GoogleMapsApi
    {
        internal RootObject GetResponse(PartyConfig partyConfig)
        {
            var locationLatitude = partyConfig.UserLocation.Latitude.ToString(CultureInfo.InvariantCulture);
            var locationLongtitude = partyConfig.UserLocation.Longitude.ToString(CultureInfo.InvariantCulture);
            var radius = partyConfig.Radius.ToString();

            using (var httpClient = new HttpClient())
            {
                var buildRequest = $"https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyAg-d-wLhMl65Fo_sfyj_U9tFOoW41UcDQ&location={locationLatitude},{locationLongtitude}&radius={radius}&type=bar";

                var response = httpClient.GetStringAsync(new Uri(buildRequest)).Result;

                return JsonConvert.DeserializeObject<RootObject>(response);
            }
        }

        public class Location
        {
            public double Lat { get; set; }
            public double Lng { get; set; }
        }

        public class Northeast
        {
            public double Lat { get; set; }
            public double Lng { get; set; }
        }

        public class Southwest
        {
            public double Lat { get; set; }
            public double Lng { get; set; }
        }

        public class Viewport
        {
            public Northeast Northeast { get; set; }
            public Southwest Southwest { get; set; }
        }

        public class Geometry
        {
            public Location Location { get; set; }
            public Viewport Viewport { get; set; }
        }

        public class Photo
        {
            public int Height { get; set; }
            public List<string> Html_attributions { get; set; }
            public string Photo_reference { get; set; }
            public int Width { get; set; }
        }

        public class PlusCode
        {
            public string Compound_code { get; set; }
            public string Global_code { get; set; }
        }

        public class OpeningHours
        {
            public bool Open_now { get; set; }
        }

        public class Result
        {
            public Geometry Geometry { get; set; }
            public string Icon { get; set; }
            public string Id { get; set; }
            public string Name { get; set; }
            public List<Photo> Photos { get; set; }
            public string Place_id { get; set; }
            public PlusCode Plus_code { get; set; }
            public double Rating { get; set; }
            public string Reference { get; set; }
            public string Scope { get; set; }
            public List<string> Types { get; set; }
            public int User_ratings_total { get; set; }
            public string Vicinity { get; set; }
            public OpeningHours Opening_hours { get; set; }
        }

        public class RootObject
        {
            public List<object> Html_attributions { get; set; }
            public string Next_page_token { get; set; }
            public List<Result> Results { get; set; }
            public string Status { get; set; }
        }
    }
}
