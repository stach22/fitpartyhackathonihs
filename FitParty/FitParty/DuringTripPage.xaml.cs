﻿using FitParty.Helpers;
using FitParty.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitParty.Models;
using FitParty.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static FitParty.Services.GoogleMapsApi;

namespace FitParty
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DuringTripPage : ContentPage
    {
        private readonly IStepCounter _stepCounter;
        private readonly Trip _trip;
        private Conversion _conversion = new Conversion();
        private Result _result;
        private RootObject _apiPlaces;
        private PartyConfig _partyConfig;

        public DuringTripPage()
        {
            InitializeComponent();
        }

        public DuringTripPage(Result result, RootObject apiPlaces, PartyConfig partyConfig, IStepCounter stepCounter)
        {
            InitializeComponent();

            _result = result;
            _apiPlaces = apiPlaces;
            _partyConfig = partyConfig;
            _stepCounter = stepCounter;
            _trip = new Trip();

            if (!_stepCounter.IsInitialized)
            {
                InitializeStepCounter();
            }

            _trip.Start = DateTime.Now;
            PlaceNameLabel.Text = _result.Name;
        }

        private void InitializeStepCounter()
        {
            _stepCounter.InitSensorService();
        }

        private async void ArrivedButton_Clicked(object sender, EventArgs e)
        {
            _trip.End = DateTime.Now;
            ConfigPartyPage.Party.CaloriesBurnt += _conversion.GetCaloriesBurnt(_trip.Start, _trip.End, App.ActiveUser.Weight);
            await Navigation.PushAsync(new ActivePartyRelaxPage(_result, _apiPlaces, _partyConfig, _stepCounter, _trip));
            switch (StartPartyPage.PartyMode)
            {
                case PartyType.Relax:
                    await Navigation.PushAsync(new ActivePartyRelaxPage(_result, _apiPlaces, _partyConfig, _stepCounter, _trip));
                    break;
                case PartyType.Survival:
                    await Navigation.PushAsync(new ActivePartySurvivalPage(_result, _apiPlaces, _partyConfig, _stepCounter, _trip));
                    break;
            }
        }
    }
}