﻿using FitParty.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FitParty.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FitParty
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StartPartyPage : ContentPage
    {
        private IStepCounter _stepCounter;
        public static PartyType PartyMode;

		public StartPartyPage ()
		{
			InitializeComponent ();
        }

        public StartPartyPage(IStepCounter stepCounter)
        {
            _stepCounter = stepCounter;
            InitializeComponent();

            Title = "Start Party";
            ((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Black;
        }

        private async void InfoButton_Clicked(object sender, EventArgs e)
        {
            var infoMessage = "Relax Mode allows you to pick bars from desired area and easily navigate to them \n" +
                "Survival Mode is designed for true hardcore drinkers, use on your own risk!";
            await DisplayAlert("INFO", infoMessage, "OK");
        }

        private async void RelaxModeButton_Clicked(object sender, EventArgs e)
        {
            PartyMode = PartyType.Relax;
            await Navigation.PushAsync(new ConfigPartyPage(_stepCounter));
        }

        private async void SurvivalModeButton_Clicked(object sender, EventArgs e)
        {
            PartyMode = PartyType.Survival;
            await Navigation.PushAsync(new ConfigPartyPage(_stepCounter));
        }
    }
}