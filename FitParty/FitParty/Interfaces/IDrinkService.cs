﻿using System.Collections.Generic;
using FitParty.Models;

namespace FitParty.Interfaces
{
    public interface IDrinkService
    {
        Drink AddCustomDrink(string name, double caloriesPerPortion, double volumeOfThePortionInGrams);
        void AddToTheListCustomDrink(string name, double caloriesPerPortion, double volumeOfThePortionInGrams);
        bool DoesCaloriesAndVolumeHaveCorrectFormat(double caloriesPerPortion, double volumeOfThePortionInGrams);
        bool DoesDrinkExist(string name);
        bool DoesDrinkNameIsCorrect(string name);
        List<Drink> GetDrinkList();
        List<Drink> ReadyDrinkList();
        List<Drink> GetChosenDrinkList();
    }
}