﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Hardware;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FitParty.Services;
using Java.Util;

namespace FitParty.Droid
{
    public class StepCounter : Java.Lang.Object, IStepCounter, ISensorEventListener
    {
        private int stepCounter = 0;
        private int counterSteps = 0;
        private int stepDetector = 0;
        private bool stepCounterInitialized = false;
        private SensorManager sManager;

        public int Steps
        {
            get => stepCounter;
            set => stepCounter = value;
        }

        public bool IsInitialized
        {
            get => stepCounterInitialized;
            set => stepCounterInitialized = value;
        }

        public new void Dispose()
        {
            sManager.UnregisterListener(this);
            sManager.Dispose();
        }

        public void InitSensorService()
        {
            sManager = Application.Context.GetSystemService(Context.SensorService) as SensorManager;
            sManager.RegisterListener(this, sManager.GetDefaultSensor(SensorType.StepCounter), SensorDelay.Normal);
            stepCounterInitialized = true;
        }

        public bool IsAvailable()
        {
            return Application.Context.PackageManager.HasSystemFeature(Android.Content.PM.PackageManager.FeatureSensorStepCounter) &&
                   Application.Context.PackageManager.HasSystemFeature(Android.Content.PM.PackageManager.FeatureSensorStepDetector);
        }


        public void OnAccuracyChanged(Sensor sensor, [GeneratedEnum] SensorStatus accuracy)
        {
            Console.WriteLine("OnAccuracyChanged called");
        }

        public void OnSensorChanged(SensorEvent e)
        {
            switch (e.Sensor.Type)
            {
                case SensorType.StepDetector:
                    stepDetector++;
                    break;
                case SensorType.StepCounter:
                    if (counterSteps < 1)
                    {
                        counterSteps = (int)e.Values[0];
                    }
                    
                    stepCounter = (int)e.Values[0] - counterSteps;
                    break;
            }
        }

        public void StopSensorService()
        {
            sManager.UnregisterListener(this);
        }
    }
}